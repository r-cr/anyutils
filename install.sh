#!/bin/sh

INSTALL="${INSTALL:-"install"}"
COPY="cp -Rf"
MKDIR="${MKDIR:-"mkdir -p"}"
DESTDIR="${DESTDIR:-${HOME}/rrr}"
PREFIX="${PREFIX:-}"
P="${P:-"anyutils"}"
BINDIR="${BINDIR:-${PREFIX}/bin}"
DOCDIR="${DOCDIR:-${PREFIX}/share/doc/${P}}"
MANDIR="${MANDIR:-${PREFIX}/share/man}"

if [ -f conf/config.sh ] ; then
    . conf/config.sh
fi

install_bin()
{
    ${MKDIR} "${DESTDIR}${BINDIR}/"
    ${INSTALL} -m555 \
        'bin/binfiles' \
        'bin/usedfiles' \
        'bin/genpatch-git' \
        'bin/srcdeb' \
        'bin/arithpkg' \
        "${DESTDIR}${BINDIR}/"
}

install_doc()
{
    ${MKDIR} "${DESTDIR}${DOCDIR}/"
    ${INSTALL} -m644 \
        'README-anyutils' \
        'License-ISC' \
        "${DESTDIR}${DOCDIR}/"
    ${COPY} \
        'doc/html/' \
        "${DESTDIR}${DOCDIR}/"

    ${MKDIR} "${DESTDIR}${MANDIR}/man1"
    ${INSTALL} -m644 \
        'doc/man/man1/binfiles.1' \
        'doc/man/man1/usedfiles.1' \
        'doc/man/man1/genpatch-git.1' \
        'doc/man/man1/srcdeb.1' \
        'doc/man/man1/arithpkg.1' \
        "${DESTDIR}${MANDIR}/man1/"
}

set -o errexit
set -o xtrace

install_bin
install_doc
