\documentclass[a4paper]{article}
\author{Random Crew}
\usepackage[utf8]{inputenc}
\usepackage{latex2man}
\usepackage{devdoc}

\setVersion{1.2}
\begin{document}

\begin{Name}{1}{arithpkg}{Random Crew}{anyutils manuals}{arithpkg reference page}

    \Prog{arithpkg}
    – the tool to compare versions of package sets.

\section{Synopsis}
    \Prog{arithpkg}
        \oOpt{-gt \Bar -ge \Bar -ge \Bar -eq \Bar -lt \Bar -le} 
        \oOpt{-N \Bar -O \Bar -C \Bar -A}\\
        \SP\SP
        \oOpt{-l}
        \oOpt{-L}
        \oOpt{-s 'char'}
        \oOpt{-S 'char'}\\
        \SP\SP
        \Opt{-f} \Arg{format}
        \Arg{ARGOLD}
        \Arg{ARGNEW}
        \oArg{ARGNEW2 .. ARGNEW\_N}

    \Prog{arithpkg}
        \Opt{-d}
        \oOpt{-l}
        \oOpt{-s 'char'}
        \oOpt{-S 'char'}
        \Opt{-f} \Arg{format}
        \Arg{ARG}
        \oArg{ARG2 .. ARG\_N}

    \Arg{input} \Bar \SP \Prog{arithpkg}
        \Arg{$<$options$>$} \Arg{ARGNEW}

    \Arg{input} \Bar \SP \Prog{arithpkg}
        \Opt{-d} \Arg{$<$options$>$}

    \Prog{arithpkg}
        \Opt{--help}\Bar\Opt{--version}

\section{Description}
    \Prog{arithpkg} compares versions of packages from 'old' set \Arg{ARGOLD} and 'new' one \Arg{ARGNEW}.
    Program prints the summary with package name, old version and new version.

    \Arg{ARGS} may be:
    \begin{itemize}
        \item[-]
            directories with packages;
        \item[-]
            package file names directly;
        \item[-]
            text lists with package versions. In that case package archives are not analyzed.
            See \Opt{-l}, \Opt{-L} options.
    \end{itemize}

    Name and version to compare are extracted from package archives. Archives are searched
    by file extension, so they may lie in nested subdirectories of directory, given in \Arg{ARG}.

    Default action is to print out the packages from the 'new' set, whose version is strictly higher
    then of the ones from 'old' set.
    Other criterias of comparison are possible, see \Opt{-gt} and grouped options.

    Several package formats are supported. See \Opt{-f} option. This option is mandatory,
    besides working entirely with text lists. Supported formats are:
    \begin{description}
        \item[deb]
            format of packages in \texttt{Debian}, \texttt{Ubuntu} and other related distros.
            Used by \Prog{dpkg} and \Prog{apt}.
        \item[txz]
            format of packages in \texttt{Slackware}. Used by \Prog{pkgtools} and \Prog{slackpkg}.
    \end{itemize}
    
    With \Opt{-d} option \Prog{arithpkg} does not compare anything, but prints the versions of single set of packages,
    given in \Arg{ARG}, to stdout.

    Input can be given through pipe instead of argument in command line.
    In that case first \Arg{ARG} for 'old' set is not given.
    When packages are compared, 'new' set still must be assigned in command line.

    \Opt{--} token is used to separate options of \Prog{arithpkg} from arguments with files or directories.


\section{Options}

\subsection{Comparison}

    Several comparison modes are possible.

    \begin{description}
        \item[ \Opt{-gt} ]
            Print package from \Arg{ARGNEW}, if its version is strictly higher then a version from \Arg{ARGOLD}.\\
            Default one.
        \item[ \Opt{-ge} ]
            Print package from \Arg{ARGNEW}, if its version is higher or equals to a version from \Arg{ARGOLD}.
        \item[ \Opt{-eq} ]
            Print package from \Arg{ARGNEW}, if its version equals to a version from \Arg{ARGOLD}.
        \item[ \Opt{-lt} ]
            Print package from \Arg{ARGNEW}, if its version is strictly lower then a version from \Arg{ARGOLD}.
        \item[ \Opt{-le} ]
            Print package from \Arg{ARGNEW}, if its version is lower or equals to a version from \Arg{ARGOLD}.
    \end{description}

\subsection{Dump}

    \begin{description}
        \item[ \Opt{-d} ]
            Print the names and versions of single set of packages to stdout. Comparison is not performed.
            Output from dump can be used as text list for further \Prog{arithpkg} input.
            \medbreak
            There may be several \Arg{ARG} arguments, but they assign single set, which will be parsed and printed.
            \medbreak
            When input is given through stdin, there are no \Arg{ARGS} in command line in this mode.

    \end{description}

\subsection{General options}

    \begin{description}
        \item[ \Opt{-f} \Arg{format}]
            Assign the format of packages to compare. Possible formats:
            \begin{description}
                \item[deb]
                \item[txz]
            \end{itemize}
            All compared packages must be of the same format.
            \medbreak
            The option is mandatory, unless only text lists are used.

        \item[ \Opt{-s} '\Arg{char}']
            Use string '\Arg{char}' as separator between package name and version in output.\\
            Default value is space symbol ' '.

        \item[ \Opt{--version} ]
            Print version and exit with 0 code.
        \item[ \Opt{--help} ]
            Print help description and exit with 0 code.
    \end{description}

\subsection{Lists}

    Input for \Prog{arithpkg} can be given with text files, or lists.
    Text files should contain package names and version, separated with space, one pair at a line.
    Text files may contain empty lines and comments, beginning with \texttt{#} symbol. Such lines are ignored.

    \begin{description}
        \item[ \Opt{-l} ]
            'old' packages are given by text file in \Arg{ARGOLD}, not by analyzing package archives.
            Note that \Arg{ARGOLD} with text list remains on its place in command line,
            after all options (unless input is piped from stdin).

        \item[ \Opt{-L} ]
            'new' packages are given by text file in \Arg{ARGNEW}, not by analyzing package archives.
            Note that \Arg{ARGNEW} with text list remains on its place in command line, after all options.

        \item[ \Opt{-S} '\Arg{char}']
            Use string '\Arg{char}' as separator between package name and version in text lists for input.\\
            Default value is space symbol ' '.
    \end{description}

\subsection{Unique packages}

    Options for assigning what to do with packages, unique for 'old' or 'new' sets.

    \begin{description}
        \item[ \Opt{-N} ]
            Print unique names for \Arg{ARGNEW}.\\
            Default one.
        \item[ \Opt{-O} ]
            Print unique names for \Arg{ARGOLD}.\\
        \item[ \Opt{-C} ]
            Do not print unique names.
        \item[ \Opt{-A} ]
            Print all unique names, from both \Arg{ARGOLD} and \Arg{ARGNEW}.
    \end{description}


\section{Examples}

    Save versions of packages from several directories and files into text file:
    \begin{verbatim}
arithpkg -f deb -d ./repo/one ./repo/two ./package-1.2.3.deb  > dump-versions.txt
    \end{verbatim}

    Compare two repos:
    \begin{verbatim}
arithpkg -f txz ./repo/one ./repo/two
    \end{verbatim}

    Compare saved list and repo:
    \begin{verbatim}
cat dump-versions.txt | arithpkg -f deb -l ./repo
    \end{verbatim}
    Or, like \texttt{tr00} unixoids:
    \begin{verbatim}
arithpkg -f deb -l ./repo < dump-versions.txt
    \end{verbatim}

    Check for old packages in \Arg{repo/two}:
    \begin{verbatim}
arithpkg -f deb -O -le ./repo/one ./repo/two
    \end{verbatim}

    Check for new packages by text lists:
    \begin{verbatim}
arithpkg -N -gt -l -L ./list-old ./list-new
    \end{verbatim}


\section{See also}
    \Cmd{diff}{1}

\section{Authors}
    Random Crew\\
%@% IF !embed %@%
    \URL{https://opendistro.org/utils}
%@% END-IF %@%

\end{Name}

\LatexManEnd
\end{document}
