usedfiles-test.log contains test log for hand-checking launch_filter function if bin/usedfiles with hard-to-read regexes.
usedfiles-result.log contains the expecting result.

If you see syscalls in your strace output, which do not match the ones in usedfiles-test.log, executable usedfiles and its launch_filter
must be expanded to match that new data.
